package assignment

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.ConfigFactory
import org.scalatest.Matchers._
import org.scalatest._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise, TimeoutException}

class AssignmentTest extends AsyncFlatSpec {

  val config = ConfigFactory.load()
  val credentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt = new AsyncBcryptImpl
  val assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }

  it should "return false if user does not exist in store" in {
    verifyCredentials("pooh", "winnie").map { result =>
      result shouldBe false
    }

  }
  it should "return false for invalid password" in {
    verifyCredentials("poohich", "wefwefweffw").map { result =>
      result shouldBe false
    }
  }

  behavior of "withCredentials"

  it should "execute code block if credentials are valid" in {
    var k = "qwerty"
    withCredentials("winnie", "pooh") {
      k = "zxc"
    }.map { _ => k shouldBe "zxc"}

  }

  it should "not execute code block if credentials are not valid" in {
    recoverToSucceededIf[InvalidCredentialsException](withCredentials("stranger", "qwerty") {
      fail("credentials are not valid but code executed")
    })
  }

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in {
    val passwords = Seq("qwe", "123")

    hashPasswordList(passwords).flatMap { x=>
      Future.sequence(x.map {
        case (password, hash) => reliableBcrypt.verify(password, hash)
      }).map{_.forall(_ == true) shouldBe true}
    }
  }

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in {
    val passwords = Seq("qwe", "123", "zxc")
    val hash = reliableBcrypt.hash("123")

    hash.flatMap(x => findMatchingPassword(passwords, x).map(res => res shouldBe Some("123")))
  }

  it should "return None if no matching password is found" in {
    val passwords = Seq("qwe", "123", "zxc")
    val hash = reliableBcrypt.hash("xcv")

    hash.flatMap(x => findMatchingPassword(passwords, x).map(res => res shouldBe None))
  }

  behavior of "withRetry"

  it should "return result on passed future's success" in {
    this.synchronized {
      withRetry(Future(10), 5).map(_ shouldBe 10)
    }
  }

  it should "not execute more than specified number of retries" in {
    val retries = 3
    @volatile var count = 0
    def calc = Future {
      count += 1
      throw new Exception
    }
    withRetry(calc, retries).recover { case _ => "match throwable"}
      .map(_ => count shouldBe retries + 1)
  }

  it should "not execute unnecessary retries" in {
    val retries = 3
    var count = 0
    def calc = Future {
      count += 1
      if (count > retries + 1) throw new IllegalArgumentException()
      else "everything is fine"
    }
    withRetry(calc, retries).map(_ shouldBe "everything is fine")
  }

  it should "return the first error, if all attempts fail" in {
    val retries = 3
    var count = 0
    def calc = Future {
      if (count == 0) throw new IllegalArgumentException()
      count += 1
      if (count > 1) throw new IndexOutOfBoundsException()
    }
    recoverToSucceededIf[IllegalArgumentException](withRetry(calc, retries))
  }

  behavior of "withTimeout"

  it should "return result on passed future success" in {
    val f = Future {Thread.sleep(1.seconds.toMillis); 100}
    withTimeout(f, 2.seconds ).map { result =>
      result shouldBe 100
    }
  }

  it should "return result on passed future failure" in {
    val f = Future {Thread.sleep(3.seconds.toMillis); 100}
    recoverToSucceededIf[TimeoutException](withTimeout(f, 2.seconds ))
  }

  it should "complete on never-completing future" in {
    def neverCompletes[T]: Future[T] = Promise[T].future
    recoverToSucceededIf[TimeoutException](withTimeout(neverCompletes, 2.seconds))
  }

  behavior of "hashPasswordListReliably"

  val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)

  it should "return password-hash pairs for successful hashing operations" in {
    val passwords = Seq("pass1","pass2","pass3","pass4","pass5")
    assignmentFlaky.hashPasswordListReliably(passwords, 3, 2.seconds).flatMap { x =>
      Future.sequence(
        x.map {
          case (password, hash) => reliableBcrypt.verify(password, hash)
        }).map{_.forall(_ == true) shouldBe true}
    }
  }
}
